﻿namespace TPLibraryAliment
{
    public class PlatAliment
    {
        private int quantite;
        private Aliment aliment;

        public int Quantite
        {
            get { return quantite; }
            set { quantite = value; }
        }

        public Aliment Aliment
        {
            get { return aliment; }
            set { aliment = value; }
        }

        public PlatAliment(Aliment aliment, int quantite)
        {
            Aliment = aliment;
            Quantite = quantite;
        }
    }
}