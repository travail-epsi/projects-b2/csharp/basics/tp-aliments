﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPLibraryAliment
{
    public class Plat
    {
        private int id;
        private string libelle;

        /*
         * Les attributs suivants indiquent le maximum ou le minimun que l'on veut pour ce plat
         * A fixer dans le constructeur
         * 
         * 
         * TODO : ajouter les bornes min, max dans le constructeur
         */
        private double lipideMax;
        private double selMax;
        private double glucideMin;
        private double proteineMin;
        private double fibreMin;

        private List<PlatAliment> aliments;

        #region Propriétés

        public int Id
        {
            get => id;
            set => id = value;
        }

        public string Libelle
        {
            get => libelle;
            set => libelle = value;
        }

        public double LipideMax
        {
            get => lipideMax;
            set => lipideMax = value;
        }

        public double SelMax
        {
            get => selMax;
            set => selMax = value;
        }

        public double GlucideMin
        {
            get => glucideMin;
            set => glucideMin = value;
        }

        public double ProteineMin
        {
            get => proteineMin;
            set => proteineMin = value;
        }

        public double FibreMin
        {
            get => fibreMin;
            set => fibreMin = value;
        }

        public List<PlatAliment> Aliments
        {
            get => aliments;
            set => aliments = value;
        }

        public string FicheDescriptive
        {
            get
            {
                return
                    $"{Id} - {Libelle} - {LipideMax} - {SelMax} - {GlucideMin} - {ProteineMin} - {FibreMin} - {Aliments}";
            }
        }

        #endregion

        #region Constructeur

        public Plat(int id, string libelle)
        {
            Id = id;
            Libelle = libelle;
        }

        public Plat(int id, string libelle, double lipideMax, double selMax, double glucideMin,
            double proteineMin, double fibreMin) : this(id, libelle)
        {
            LipideMax = lipideMax;
            SelMax = selMax;
            GlucideMin = glucideMin;
            ProteineMin = proteineMin;
            FibreMin = fibreMin;
        }

        #endregion

        #region CRUD

        // Ajouter un platAliment : le C dans CRUD
        public bool AjouterPlatAliment(PlatAliment platAliment)
        {
            if (platAliment == null)
                return false;

            /** TODO : faire la recherche */

            /** TODO : Ajouter uniquement si la contrainte des bornes min, max sont respectées */
            aliments.Add(platAliment);
            return true;
        }

        // Supprimer un platAliment : le D dans CRUD
        public bool SupprimerPlatAliment(PlatAliment platAliment)
        {
            /** TODO : faire l'appel à la méthode de suppression  SupprimerPlatAliment(int id) */
        }

        public bool SupprimerPlatAliment(int id)
        {
            PlatAliment p = RechercherPlatAliment(id);
            if (p == null)
                return false; // Le platAliment n'existe pas

            aliments.Remove(p);
            return true;
        }

        // Modifier un platAliment : le U dans CRUD
        public bool ModifierPlatAliment(PlatAliment platAliment)
        {
            if (platAliment == null)
                return false;

            /** TODO : corriger la recherche
            PlatAliment platAlimentAModifier = RechercherPlatAliment(platAliment.Id);
            if (platAlimentAModifier == null)
                return false; // Le platAliment n'existe pas
            */

            // On fait des modification

            /** TODO : Faire la modification de la quantité */
            return true;
        }

        #endregion

        #region Méthodes de recherche

        public PlatAliment RechercherPlatAliment(int id)
        {
            /** TODO : faire la recherche */
        }

        #endregion

        /** TODO : faire toutes les méthodes suivantes : */
        public double TotalLipides()
        {
            return 0;
        }

        public double TotalGlucides()
        {
            return 0;
        }

        public double TotalProteines()
        {
            return 0;
        }

        public double TotalFibres()
        {
            return 0;
        }

        public double TotalSels()
        {
            return 0;
        }

        public double TotalCalorie()
        {
            return 0;
        }

        public double Poids()
        {
            return 0;
        }
    }
}