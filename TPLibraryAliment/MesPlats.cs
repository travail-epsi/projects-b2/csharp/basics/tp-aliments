﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPLibraryAliment
{
    public class MesPlats
    {
        #region Attributs
        private List<Plat> listePlats = new List<Plat>();
        #endregion

        #region CRUD
        // Ajouter un plat : le C dans CRUD
        public bool AjouterPlat(Plat plat)
        {
            /** TODO : A faire - Ajouter le plat s'il n'existe pas déjà */
        }

        // Supprimer un plat : le D dans CRUD
        public bool SupprimerPlat(Plat plat)
        {
            return SupprimerPlat(plat.Id);
        }
        public bool SupprimerPlat(int id)
        {
            /** TODO : A faire */
        }

        // Modifier un plat : le U dans CRUD
        public bool ModifierPlat(Plat plat)
        {
            /** TODO : A faire - Il ne faut modifer que la quantité */
        }
        #endregion

        #region Méthodes de recherche
        public Plat RechercherPlat(int id)
        {
            /** TODO : A faire */
        }

        public List<Plat> RechercherPlatsLibelleCommencePar(string libelle)
        {
            /** TODO : A faire */
        }

        public List<Plat> RechercherPlatsParCalorie(double min, double max)
        {
                /** TODO : A faire 
                 * 
                 * Retourne tous les plats dont les calories sont dans l'intervalle indiqué
                 * 
                 **/
        }
        #endregion


        public void AfficherPlats()
        {
            foreach (Plat p in listePlats)
            {

                Console.WriteLine(p.FicheDescriptive);
            }
        }
    }
}
